/**
 * 
 */
package com.ditty.controller;

/**
 * @author dingnate
 *
 */
public class ControllerFactory {
	public static Object getController(Class<?> clazz) throws Exception {
		return clazz.newInstance();
	}
}
