/**
 * 
 */
package com.ditty.handler;

import java.util.ArrayList;
import java.util.List;

import com.ditty.kit.LinkedKit;

/**
 * @author dingnate
 *
 */
public final class Handlers {
	private static Handlers ME = new Handlers();
	private List<AbstractHttpHandler> handlers = new ArrayList<AbstractHttpHandler>();
	private ActionHandler actionHandler;
	private AbstractHttpHandler head;

	Handlers() {
	}

	public static Handlers me() {
		return ME;
	}

	public synchronized AbstractHttpHandler buildHandler() {
		if (head == null) {
			head = LinkedKit.buildLinkedObjects(handlers, actionHandler);
		}
		return head;
	}

	public Handlers addHandler(AbstractHttpHandler handler) {
		handlers.add(handler);
		return this;
	}

	/**
	 * @param actionHandler the actionHandler to set
	 * @return 
	 */
	public final Handlers setActionHandler(ActionHandler actionHandler) {
		this.actionHandler = actionHandler;
		return this;
	}

	/**
	 * @return the head
	 */
	public final AbstractHttpHandler getHead() {
		if (head == null) {
			return buildHandler();
		}
		return head;
	}

	/**
	 * @param head the head to set
	 * @return 
	 */
	public final Handlers setHead(AbstractHttpHandler head) {
		this.head = head;
		return this;
	}
}
