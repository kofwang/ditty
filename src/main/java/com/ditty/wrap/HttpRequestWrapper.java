/**
 * 
 */
package com.ditty.wrap;

import java.util.HashMap;
import java.util.Map;

import com.ditty.kit.ValueKit;

/**
 * @author dingnate
 *
 */
public class HttpRequestWrapper implements IHttpRequestWrapper {
	private String method;
	private Map<String, String> headers;
	private String uri;
	private byte[] content;

	public HttpRequestWrapper(String method, String uri, Map<String, String> headers, byte[] content) {
		this.method = ValueKit.getValue(method, "GET");
		this.uri = uri;
		this.headers = ValueKit.getValue(headers, new HashMap<String, String>());
		this.content = content;
	}

	@Override
	public Map<String, String> headers() {
		return headers;
	}

	@Override
	public String method() {
		return method;
	}

	@Override
	public String uri() {
		return uri;
	}

	@Override
	public byte[] content() {
		return content;
	}
}
