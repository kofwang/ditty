package com.ditty.server;

import com.ditty.server.netty.NettyServer;

/**
 * @author dingnate
 *
 */
public class ServerFactory implements IServerFactory {
	private final static ServerFactory ME = new ServerFactory();
	public static ServerFactory me() {
		return ME;
	}

	@Override
	public IServer getServer() {
		return getServer(ServerType.NETTY);
	}

	@Override
	public IServer getServer(ServerType serverType) {
		return new NettyServer();
	}
}
