/**
 * 
 */
package com.ditty.bean;

/**
 * 响应体
 * @author dingnate
 *
 */
public class ResponseBody {
	private Object returnValue;

	public ResponseBody() {
	}

	/**
	 * @param returnValue
	 */
	public ResponseBody(Object returnValue) {
		super();
		this.returnValue = returnValue;
	}

	/**
	 * @return the returnValue
	 */
	public final Object getReturnValue() {
		return returnValue;
	}

	/**
	 * @param returnValue the returnValue to set
	 * @return 
	 */
	public final ResponseBody setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
		return this;
	}

}
