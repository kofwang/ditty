/**
 * 
 */
package com.ditty.kit;

/**
 * @author dingnate
 *
 */
public class JdkKit {
	public static final String V_1_7 = "1.7";
	public static String getVersion() {
		return System.getProperty("java.version", "");
	}
	public static boolean isGteVersion(String version) {
		return JdkKit.getVersion().compareTo(version) >= 0;
	}
}
