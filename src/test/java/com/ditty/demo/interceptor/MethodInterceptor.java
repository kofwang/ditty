/**
 * 
 */
package com.ditty.demo.interceptor;

import com.ditty.interceptor.AbstractActionIntercptor;
import com.ditty.invoke.action.ActionInvoker;

/**
 * @author dingnate
 *
 */
public class MethodInterceptor extends AbstractActionIntercptor {
	@Override
	public void interceptor(ActionInvoker invoker) {
		System.out.println(getClass().getName()+":in");
		invoker.inovke();
		System.out.println(getClass().getName()+":out");
	}
}
