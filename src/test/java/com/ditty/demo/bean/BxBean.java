package com.ditty.demo.bean;

import java.util.Date;


/**
 * @author dingnate
 *
 */
public class BxBean {
	int a;
	boolean b;
	Date d;
	public BxBean() {
	}
	/**
	 * @return the d
	 */
	public final Date getD() {
		return d;
	}
	/**
	 * @param a
	 * @param b
	 * @param d
	 * @param s
	 */
	public BxBean(int a, boolean b, Date d, String s) {
		super();
		this.a = a;
		this.b = b;
		this.d = d;
		this.s = s;
	}
	/**
	 * @param d the d to set
	 */
	public final void setD(Date d) {
		this.d = d;
	}
	/**
	 * @return the a
	 */
	public final int getA() {
		return a;
	}
	/**
	 * @param a
	 *            the a to set
	 */
	public final void setA(int a) {
		this.a = a;
	}
	/**
	 * @return the b
	 */
	public final boolean isB() {
		return b;
	}
	/**
	 * @param b
	 *            the b to set
	 */
	public final void setB(boolean b) {
		this.b = b;
	}
	/**
	 * @return the s
	 */
	public final String getS() {
		return s;
	}
	/**
	 * @param s
	 *            the s to set
	 */
	public final void setS(String s) {
		this.s = s;
	}
	String s;
}
