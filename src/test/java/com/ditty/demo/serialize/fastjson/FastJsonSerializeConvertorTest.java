/**
 * 
 */
package com.ditty.demo.serialize.fastjson;

import java.util.Date;
import java.util.List;

import com.ditty.demo.bean.AxBean;
import com.ditty.demo.bean.BxBean;
import com.ditty.kit.CharsetKit;
import com.ditty.serialize.ISerializeConvertor;
import com.ditty.serialize.SerializeConvertorFactory;
import com.ditty.serialize.fastjson.FastJsonSerializeConvertor;

/**
 * @author dingnate
 *
 */
public class FastJsonSerializeConvertorTest extends FastJsonSerializeConvertor {

	public static void main(String[] args) {
		ISerializeConvertor convertor = SerializeConvertorFactory.getSerializeConvertor();
		BxBean bBean = new BxBean(1, true, new Date(), "ss");
		byte[] bytes = convertor.serialize(bBean);
		System.out.println(new String(bytes, CharsetKit.DEFAULT_CHARSET));
		
		bytes = convertor.serialize(new AxBean(11, false, "s", bBean));
		System.out.println(new String(bytes, CharsetKit.DEFAULT_CHARSET));
		AxBean a = convertor.deserialize(bytes, AxBean.class);
		System.out.println(a);

		byte[] bytes1 = convertor.serialize(new AxBean[] { new AxBean(11, false, "s", bBean) });
		System.out.println(new String(bytes1, CharsetKit.DEFAULT_CHARSET));
		List<AxBean> a1 = convertor.deserializeArray(bytes1, AxBean.class);
		System.out.println(a1);
	}
}
